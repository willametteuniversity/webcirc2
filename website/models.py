from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db import models

# Create your models here.

class CommonEquipmentInfo(models.Model):
    cur_room_num = models.IntegerField()
    cur_building = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    inventory_num = models.IntegerField()
    make = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    perm_building = models.CharField(max_length=100)
    perm_room_num = models.IntegerField()
    perm_bin_num = models.IntegerField()
    purchase_date = models.DateField()
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2)
    serial = models.CharField(max_length=100)
    warranty_expiration_date = models.DateField(null=True, blank=True)
    labels = generic.GenericRelation(Label)
   
    class Meta:
        abstract = True

class Label(models.Model):
    label_text = models.CharField(max_length=100)
    equipment = generic.GenericForeignKey('content_type', 'object_id')
 
